# Changelog

## [0.2] - 2021-01-14

### Added

- Tft wrapper class
- New graphic for steam temperature and target steam temperature
- Images on separate .h
- Changelog

### Changed
- Forced 5s delay while booting for the (un)neccessary ad text and gitlab link :)
- Cleaned up some commented out lines
- New position for the timers switching automatically between current timer (if any) and last shot timer
- BLE logo on to right, heating logo moved above the new steam graphic




## [0.1] - 2021-01-10
- everything else 