# Irrelevant stuff
Some details about the connection with the PID.
I don't speak UART, so feel free to correct me.

 
 ## CN10 connector
<img src="marax_pid_connector_photo.jpg" width="200">  
<img src="marax_pid_connector.jpg" width="200">  

- RED: 12V
- BLACK: GND
- BLUE: RX
- YELLOW: TX
- WHITE: RTS*
- PURPLE: CTS*


\* or vise versa, didn't keep notes, sorry  

**connect RX/TX and ground**  
scope says signal is 5V so uart2 pins of esp32 (GPIO16, GPIO17) are safe


## UART highs and lows  
<img src="logic/dsview4.png" width="450">  

**more pics in the "logic" folder.**
 
For more details on the 1s and the 0s, download the DSlogic .dsl file and open it with [DSView Software](https://www.dreamsourcelab.com/download/)   
      
        
*ps. Really sorry about the macosx, I'm not that kind of person,it was infront of me at the time, bad luck. Next time I promise will be from linux or at least windows :D*
