# Mara X Visualizer

You don't have to use a TFT/OLED display.  
You can only use a MCU pcb [**and connect to it with the mobile app.**](https://gitlab.com/jousis/MaraXMobile)  

 
[![Youtube demo](https://img.youtube.com/vi/5fiiuA-9tAk/0.jpg)](https://youtu.be/5fiiuA-9tAk "")
<img src="media/Screenshot_20210123-210733_MaraXMobile.jpg" width="200"> 
<br/><br/><br/>
## New UI elements  
<img src="media/P1150093.jpg" width="200">  

- Boost value column (red column)
- Steam temp graph with current temp (bottom) , target temp (top) and heating icon (changed depending on boost)
- 2 timers, the current gets center stage when > 5s, else, the "last" shot value is shown.
 

## Temp response (data from Adafruit IO)
[Mara X Temperature during warmup](./docs/TEMP_RESPONSE_WARMUP.md)   
[Mara X Temperature before/during/after 2shots + milk](./docs/TEMP_RESPONSE_2SHOTS.md)  
[Mara X Temperature before/during/after a cappuccino](./docs/TEMP_RESPONSE.md)   
[<img src="media/marax_adafruitio_edit.jpg" width="400"> ](./docs/TEMP_RESPONSE.md)  
    

## Progress

| feature | status | notes |  
| --- | --- | --- |  
| BLE | DONE | Ionic App released |
| WiFi | DONE | wifi name/pass can be set through BLE |
| REST API | TODO | |
| Adafruit IO | DONE | using MQTT |
| Detect pump | DONE | PIN 25 |
| TFT 2.4" Featherwing | DONE |  |
| OLED 128x128 UI | TODO |  |
| OLED 128x64 UI | DONE | Basic UI |


## Goal

Display all the parameters available on the screen and keep a buffer of the last X temp values.  
Transmit all the parameters and the buffer through BLE to a mobile app.

## Similar projects

Mara X Timer by alexrus [Link](https://github.com/alexrus/marax_timer)  
Data Visualisation - Lelit MaraX 'Mod' by calin__ [Link](https://www.reddit.com/r/espresso/comments/hft5zv/data_visualisation_lelit_marax_mod/)


## Hardware used

Adafruit HUZZAH32 (ESP32)  [Link](https://www.adafruit.com/product/3405)  
TFT FeatherWing - 2.4" 320x240 Touchscreen [Link](https://www.adafruit.com/product/3315)


## Connection with Mara X
A simple dupont cable. Mara exposes RX/TX of the PID, connect the cable to the middle 2 pins of the debug interface (has 6 pins).  
Reddit post of calin__ has photos of the internals.


## License
This project is licensed under the MIT License.[Learn more](http://choosealicense.com/licenses/mit/)
