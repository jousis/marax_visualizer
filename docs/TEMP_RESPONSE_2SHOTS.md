# Temperature during 2 shots + 1 milk

<img src="../media/marax_2shots.jpg" width="1024">

## Source data

[Excel file (incl graphs)](./TempResponseGraph-2shots.xlsx)  
[CSV files](../data/)  


## Notes
- 2 shots back to back and milk afterwards. No cleaning/flushing.
- Ambient temp 22-23C
- 2nd shot was slightly quicker, hx temp graph shows higher temp reached.
- Difference shown in data had no effect in taste.
- 1st shot, 20g->37g @ 34s (start @ 94C)
- 2ns shot, 20g->38g @ 32s (start @ 93C)


## Important timestamps
- 3:55:08 (0 min) 1st shot begins
- 3:57:48 (2 min) 2ns shot begins
- 4:00:00 (4 min) milk preparation
