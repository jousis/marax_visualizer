# Temperature during warmup
 
<img src="../media/marax_warmup_adafruitio.jpg" width="1024">

## Source data

[Excel file (incl graphs)](./TempResponseGraph-Warmup.xlsx)  
[CSV files](../data/)  


## Notes
- Data logging begins from the poweron of the machine (2:32:08)
- 1st warmup of the day, machine was cold
- Ambient temp 22-23C
- Ready to make coffee ~30min from power on


## Important timestamps
- 2:32:08 (00 min) startup
- 2:32:28 (00 min) PID wakes up and begins boosting steam tem
- 2:45:28 (13 min) End of max boost period (Steam at max temp)
- 2:50:38 (18 min) Max water temp (start)
- 2:54:08 (22 min) Max water temp (end)
- 3:04:18 (32 min) 1st time on target water temp (94C)
- 3:05:08 (33 min) PID enters control phase (heating element on/off but steam target temp is unchanged)

## Heating cycle (stable phase)
- When temp reaches stable-low (93C), steam temp is 113C, PID starts heating
- Steam goes up to 117C and hx temp goes up to 94C, heating is stopped (or reduced)
- period is 8min (4min@93C, 4min@94C)