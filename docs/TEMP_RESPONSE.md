# Temperature during 1 shot + milk
 
<img src="../media/marax_adafruitio_edit.jpg" width="1024">

## Source data

[Excel file (incl graphs)](./TempResponseGraph-Cappuccino.xlsx)  
[CSV files](../data/)  


## More info on temp response of Coffee Mode 1
[**See the back2back shots graph also**](./TEMP_RESPONSE_2SHOTS.md)
- HX temp is hovering between 93C-94C after the initial warmup (1h here)
- I begin a proper shot, relatively long, 40s. Without any flush.
- During the shot the controller boosts, (i guess) in order to keep the temp of the extraction water in check. We see a huge spike in HX temp reading and at the end of the shot a sharp drop.
- At the start of the shot, Steam Target temp get a +24C target (!) and when the shot ends, goes briefly to +12C and then hovers between +12 and +0 as I prepare the milk.
- The above behavior has as a result an impressive temp recovery of HxTemp and...
- 2min after the shot we are back at 93C (hx)
- 4min after the shot, we are at a temporary stable phase, around 95C, which holds impressively stable for the next 10min
- Then we enter a very strange steam boosting phase which will ruin any espresso. You cannot make coffee for the next 15min, temp goes up to 100C and slowly drops to 94C.
- Finally we re enter the impressively stable phase and temp hovers between 93C-94C


## Thoughts
- Best time to make coffee is during the stable phase when the machine is sitting all warmed up after 30min.
- You can make excellent coffee 2-3min after the shot.
- You still have time for coffee after 3rd minute, but temp will be +2C higher (not sure about the cup).
- Although slightly higher (95C here), temp is very stable from 4min mark up to 14min.
- Then, the machine is useless for espresso for the next 15min (unless you do a cooling flush I guess).


