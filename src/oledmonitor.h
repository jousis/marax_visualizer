#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET -1
#define SCREEN_ADDRESS 0x3C
#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64

#define DISPLAY_WHITE 1
#define DISPLAY_YELLOW 1
#define DISPLAY_BLACK 0

class OLED_MONITOR
{
public:
    OLED_MONITOR();

    void initialize();
    void printInfo();
    void blank();
    void sleep();
    void wakeup();
    void drawHxTemp(int value, bool skipText, bool forceRedraw);

    void drawSteamTemps(int realTemp, int targetTemp);

    void drawAngle(int angle, int v, int r, int w, int x, int y);
    void drawTimers(uint32_t realtime, uint32_t last, bool priorityOnRealtime);
    void drawBleLogo(bool show);
    void drawWiFiLogo(bool show);
    void drawHeatingIcon(bool heatingEnabled, uint16_t heatingBoostValue);
    void drawSteamBoost(uint16_t heatingBoostValue);
    void drawText(String inputString, uint8_t textSize, uint16_t color, uint16_t bg_color, int16_t x, int16_t y);
    void drawPumpIcon(bool status);

protected:
    const int heatingIconX = 0;
    const int heatingIconY = 0;
    const int pumpLogoX = 24;
    const int pumpLogoY = 6;
    const int bleLogoX = 128 - 24;
    const int bleLogoY = 0;
    const int wifiLogoX = 128 - 48;
    const int wifiLogoY = 0;

    String makeStringGreatAgain(String inputstr, int length);
    Adafruit_SSD1306 display = Adafruit_SSD1306(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
};