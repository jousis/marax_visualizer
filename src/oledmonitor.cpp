#include <oledmonitor.h>
#include <images.h>

OLED_MONITOR::OLED_MONITOR()
{
}

void OLED_MONITOR::initialize()
{
    Serial.println("will init OLED I2C");
    if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS))
    {
        Serial.println(F("SSD1306 allocation failed"));
        for (;;)
            ; // Don't proceed, loop forever
    }
    display.clearDisplay();
    display.display();
};

void OLED_MONITOR::printInfo()
{
    display.clearDisplay();
    drawText("Lelit Mara X", 2, DISPLAY_WHITE, DISPLAY_BLACK, 0, 0);
    drawText("#OpenSource", 1, DISPLAY_WHITE, DISPLAY_BLACK, 0, 16);
    drawText("#IoT", 1, DISPLAY_WHITE, DISPLAY_BLACK, 0, 26);
    drawText("#ESP32", 1, DISPLAY_WHITE, DISPLAY_BLACK, 0, 36);
    drawText("https://gitlab.com/jousis/", 1, DISPLAY_WHITE, DISPLAY_BLACK, 0, 46);
}

void OLED_MONITOR::blank()
{
    display.clearDisplay();
    display.drawLine(74, 16, 74, 63, SSD1306_WHITE);
    display.display();
}

void OLED_MONITOR::sleep()
{
    display.clearDisplay();
};
void OLED_MONITOR::wakeup()
{
    display.drawLine(74, 16, 74, 63, SSD1306_WHITE);
};

void OLED_MONITOR::drawText(String inputString, uint8_t textSize, uint16_t color, uint16_t bg_color, int16_t x, int16_t y)
{
    display.setCursor(x, y);
    display.setTextSize(textSize);
    display.setTextColor(color, bg_color);
    display.println(inputString);
    display.display();
}

String OLED_MONITOR::makeStringGreatAgain(String inputstr, int length)
{
    String output = inputstr;
    for (int i = inputstr.length(); i < length; i++)
    {
        output = " " + output;
    }
    if (output.length() > length)
    {
        output = output.substring(0, length);
    }
    return output;
}

void OLED_MONITOR::drawHxTemp(int value, bool skipText, bool forceRedraw)
{
    drawText(makeStringGreatAgain(String(value) + "C", 4), 3, DISPLAY_WHITE, DISPLAY_BLACK, 1, 18);
}

void OLED_MONITOR::drawSteamTemps(int realTemp, int targetTemp)
{
    drawText(makeStringGreatAgain(String(realTemp) + "C", 4), 2, DISPLAY_WHITE, DISPLAY_BLACK, 24, 48);
}

void OLED_MONITOR::drawSteamBoost(uint16_t heatingBoostValue)
{
}

void OLED_MONITOR::drawTimers(uint32_t realtime, uint32_t lastTimer, bool priorityOnRealtime)
{
    static uint32_t oldRT = 0;
    static uint32_t oldLT = 0;
    static bool oldPR = true;

    if (realtime == oldRT && lastTimer == oldLT && priorityOnRealtime == oldPR)
    {
        return;
    }

    float bigTimerFloat;
    float smallTimerFloat;

    if (priorityOnRealtime)
    {
        bigTimerFloat = (float)realtime / 1000;
        smallTimerFloat = (float)lastTimer / 1000;
    }
    else
    {
        bigTimerFloat = (float)lastTimer / 1000;
        smallTimerFloat = (float)realtime / 1000;
    }

    int xPrimaryText = 80;
    int xSecondaryText = xPrimaryText;
    int yPrimaryText = 18;
    int ySecondaryText = 48;

    display.setTextColor(DISPLAY_WHITE, DISPLAY_BLACK);
    display.setTextSize(2);
    display.setCursor(xPrimaryText, yPrimaryText);
    display.print(makeStringGreatAgain(String(bigTimerFloat, 1), 4));

    display.setTextColor(DISPLAY_WHITE, DISPLAY_BLACK);
    display.setTextSize(2);
    display.setCursor(xSecondaryText, ySecondaryText);
    display.print(makeStringGreatAgain(String(smallTimerFloat, 1), 4));

    // if (!priorityOnRealtime)
    // {
    //     display.fillRect(xSecondaryText + 8, ySecondaryText - 15, 60, 10, DISPLAY_BLACK);
    //     display.setTextColor(DISPLAY_WHITE, DISPLAY_BLACK);
    //     display.setTextSize(2);
    //     display.setCursor(xPrimaryText + 25, yPrimaryText - 20);
    //     display.print("LAST SHOT");
    // }
    // else
    // {
    //     display.fillRect(xPrimaryText + 25, yPrimaryText - 20, 120, 18, DISPLAY_BLACK);
    //     display.setTextColor(DISPLAY_WHITE, DISPLAY_BLACK);
    //     display.setTextSize(1);
    //     display.setCursor(xSecondaryText + 8, ySecondaryText - 15);
    //     display.print("LAST SHOT");
    // }

    oldRT = realtime;
    oldLT = lastTimer;
    oldPR = priorityOnRealtime;
}

void OLED_MONITOR::drawWiFiLogo(bool show)
{
    static bool wifiLogoVisible = false;
    if (show && !wifiLogoVisible)
    {
        drawText(String("W"), 2, DISPLAY_WHITE, DISPLAY_BLACK, wifiLogoX, wifiLogoY);
        wifiLogoVisible = true;
    }
    if (!show && wifiLogoVisible)
    {
        drawText(String(" "), 2, DISPLAY_WHITE, DISPLAY_BLACK, wifiLogoX, wifiLogoY);
        wifiLogoVisible = false;
    }
}

void OLED_MONITOR::drawBleLogo(bool show)
{
    static bool bleLogoVisible = false;
    if (show && !bleLogoVisible)
    {
        drawText(String("B"), 2, DISPLAY_WHITE, DISPLAY_BLACK, bleLogoX, bleLogoY);
        bleLogoVisible = true;
    }
    if (!show && bleLogoVisible)
    {
        drawText(String(" "), 2, DISPLAY_WHITE, DISPLAY_BLACK, bleLogoX, bleLogoY);
        bleLogoVisible = false;
    }
}

void OLED_MONITOR::drawPumpIcon(bool status)
{
    if (status)
    {
        display.fillCircle(pumpLogoX, pumpLogoY, 4, DISPLAY_WHITE);
    }
    else
    {
        display.fillCircle(pumpLogoX, pumpLogoY, 8, DISPLAY_BLACK);
    }
}

// void OLED_MONITOR::drawHeatingIcon(bool heatingEnabled, uint16_t heatingBoostValue)
// {
//     static bool heatingLogoVisible = false;
//     if (heatingEnabled && !heatingLogoVisible)
//     {
//         if (heatingBoostValue > 0)
//         {
//             display.fillCircle(heatingIconX, heatingIconY / 2, 8, DISPLAY_WHITE);
//         }
//         drawText(String("H"), 2, DISPLAY_WHITE, DISPLAY_BLACK, heatingIconX, heatingIconY);
//         heatingLogoVisible = true;
//     }
//     if (!heatingEnabled && heatingLogoVisible)
//     {
//         drawText(String(" "), 2, DISPLAY_WHITE, DISPLAY_BLACK, heatingIconX, heatingIconY);
//         heatingLogoVisible = false;
//     }
// }

void OLED_MONITOR::drawHeatingIcon(bool heatingEnabled, uint16_t heatingBoostValue)
{
    static int heatingLogoStyle = 0; //0 = disabled, 1 = orange single , 2 = red single, 3 = red+orange stacked
    if (heatingEnabled)
    {
        if (heatingBoostValue > 1000 && heatingLogoStyle != 3)
        {
            heatingLogoStyle = 3;
            display.fillRect(heatingIconX, heatingIconY, 32, 16, DISPLAY_BLACK);
            display.drawBitmap(heatingIconX, heatingIconY, heatingLogoSmall, 12, 12, DISPLAY_WHITE);
            display.drawBitmap(heatingIconX + 1, heatingIconY, heatingLogoSmall, 12, 12, DISPLAY_BLACK);
            display.drawBitmap(heatingIconX + 3, heatingIconY, heatingLogoSmall, 12, 12, DISPLAY_WHITE);
            display.drawBitmap(heatingIconX + 4, heatingIconY, heatingLogoSmall, 12, 12, DISPLAY_BLACK);
            display.drawBitmap(heatingIconX + 6, heatingIconY, heatingLogoSmall, 12, 12, DISPLAY_WHITE);
        }
        else if (heatingBoostValue < 500 && heatingLogoStyle != 1)
        {
            heatingLogoStyle = 1;
            display.fillRect(heatingIconX, heatingIconY, 32, 16, DISPLAY_BLACK);
            display.drawBitmap(heatingIconX, heatingIconY, heatingLogoSmall, 12, 12, DISPLAY_WHITE);
        }
        else if ((heatingBoostValue >= 500 && heatingBoostValue <= 1000) && heatingLogoStyle != 2)
        {
            heatingLogoStyle = 2;
            display.fillRect(heatingIconX, heatingIconY, 32, 16, DISPLAY_BLACK);
            display.drawBitmap(heatingIconX, heatingIconY, heatingLogoSmall, 12, 12, DISPLAY_WHITE);
            display.drawBitmap(heatingIconX + 1, heatingIconY, heatingLogoSmall, 12, 12, DISPLAY_BLACK);
            display.drawBitmap(heatingIconX + 3, heatingIconY, heatingLogoSmall, 12, 12, DISPLAY_WHITE);
        }
    }
    if (!heatingEnabled && heatingLogoStyle > 0)
    {
        heatingLogoStyle = 0;
        display.fillRect(heatingIconX, heatingIconY, 32, 16, DISPLAY_BLACK);
    }
}