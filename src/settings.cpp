#include "settings.h"
// #include "nvs_flash.h"

SETTINGS::SETTINGS()
{
    // Initialize NVS.
    // nvs_flash_init();
    // loadSettings();
}

void SETTINGS::clearEEPROM()
{
    preferences.begin("marax", false); //RW
    preferences.clear();
    preferences.end();
}

void SETTINGS::resetSettings()
{
    //we only reset the values to default, not applying them.
    //If you want immediate change, you need to call applySetting(String key,String value, bool saveToEEPROM) for each one
    wifiHostname = DEFAULT_WIFI_HOSTNAME;
    wifiSSID = DEFAULT_WIFI_SSID;
    wifiPassword = DEFAULT_WIFI_PASSWORD;
    autoConnectWiFi = 0;
    saveSettings();
}

bool SETTINGS::loadSettings()
{
    preferences.begin("marax", true); //read only
    uint8_t firstrun = preferences.getUChar("firstrun", 1);

    if (firstrun == 1)
    {
        preferences.end();
        return false;
    }
    Serial.print("Free entries ");
    Serial.println(preferences.freeEntries());
    wifiHostname = preferences.getString("wifiHostname", DEFAULT_WIFI_HOSTNAME);
    wifiSSID = preferences.getString("wifiSSID", DEFAULT_WIFI_SSID);
    wifiPassword = preferences.getString("wifiPassword", DEFAULT_WIFI_PASSWORD);
    autoConnectWiFi = preferences.getBool("autoConnectWiFi", 0);
    preferences.end();
    return true;
}

void SETTINGS::saveSettingString(const char *key, String value)
{
    preferences.begin("marax", false); //RW
    preferences.putString(key, value);
    preferences.end();
}

void SETTINGS::saveSettingByte(const char *key, uint8_t value)
{
    preferences.begin("marax", false); //RW
    preferences.putUChar(key, value);
    preferences.end();
}

void SETTINGS::saveSettings()
{
    //saving all settings to eeprom
    preferences.begin("marax", false); //RW

    Serial.println("Clearing EEPROM");
    preferences.clear();

    preferences.putString("wifiHostname", wifiHostname);
    preferences.putString("wifiSSID", wifiSSID);
    preferences.putString("wifiPassword", wifiPassword);

    preferences.putUChar("autoConnectWiFi", autoConnectWiFi);
    preferences.putUChar("firstrun", 0);

    preferences.end();
}