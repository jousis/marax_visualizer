#include <Arduino.h>
#include <CircularBuffer.h>
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
#include <settings.h>
#include <images.h>

/////////////////////////////////////////////////
//
// CHOOSE THE CORRECT BOARD IN platformio.ini !!!
//
/////////////////////////////////////////////////
// esp32thing for sparkfunesp32 or clones, featheresp32 for adafruit huzzah32, ...
// If you cannot see anything on the monitor, this might be the problem
/////////////////////////////////////////////////

// PUMP detection //
// defaults are for reed attached to pump, you can use anything else
#define RESISTOR_TYPE INPUT_PULLUP // pullup for reed or similar sensors. Change to pulldown if needed, depending on your sensor
const bool IS_REED = true;         //it will enable logic needed for reed sensor
#define PUMP_PIN 25                //GPIO 25, A1 on Huzzah32

/////////////////////////////////////////////////
//// DISPLAY SELECTION
/////////////////////////////////////////////////
//// 2.4" featherwing TFT (ILI9341)
////--> add adafruit/Adafruit ILI9341 library
////--> and adafruit/Adafruit BusIO library
#define FEATHERWING_TFT

////I2C 128*64 OLED (2 or 1 color)
////--> Connect the I2C monitor to GND,3V3 pin and for I2C , SDA of monitor to GPIO21 and SCL to GPIO22
////--> add adafruit/Adafruit SSD1306 library
////--> and adafruit/Adafruit BusIO library
// #define OLED_128_64
/////////////////////////////////////////////////

/////////////////////////////////////////////////
//// DEMO
/////////////////////////////////////////////////
//Enable if you want to test all the bling having various fake temp data
// #define DEMO
/////////////////////////////////////////////////

/////////////////////////////////////////////////
//// WIFI
//// Enable for REST,MQTT or AdafruitIO MQTT functionality
// #define USE_WIFI
/////////////////////////////////////////////////

/////////////////////////////////////////////////
//// ADAFRUIT IO
/////////////////////////////////////////////////
//// For ADAFRUIT IO you will need to rename aioconfig_sample.h to aioconfig.h file and edit the parameters inside
//// Then , in your AIO panel, create 3 feeds: hxtemp,steamtemp and steamtargettemp
//// Finaly, enable the following
// #define USE_ADAFRUIT_IO
/////////////////////////////////////////////////
//// If you do not want AIO enabled, go to platformio.ini and disable the libraries:
//// adafruit / Adafruit IO Arduino
//// adafruit / Adafruit MQTT Library
//// arduino - libraries / ArduinoHttpClient
/////////////////////////////////////////////////

#if defined(FEATHERWING_TFT)
#include <tftmonitor.h>
TFT_MONITOR display = TFT_MONITOR();
#endif

#if defined(OLED_128_64)
#include <oledmonitor.h>
OLED_MONITOR display = OLED_MONITOR();
#endif

#if defined(USE_WIFI)
#include <WiFi.h>
WiFiClient client;
bool wifiActivated = false; //set to true if wifi connection is established on setup();
// WiFiServer server(80);
static uint8_t WIFI_CONNECTION_TIMEOUT = 20; //in seconds, if we do not connect within 30s, reboot
#endif

#if defined(USE_ADAFRUIT_IO)
#include <aioconfig.h>
#include <AdafruitIO_WiFi.h>
#include <Adafruit_MQTT.h>
#include <Adafruit_MQTT_Client.h>

Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY);
Adafruit_MQTT_Publish hxtempfeed = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/hxtemp");
Adafruit_MQTT_Publish stempfeed = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/steamtemp");
Adafruit_MQTT_Publish sttempfeed = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/steamtargettemp");

uint32_t lastAdafruitIORefreshMillis = 0;

#endif

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

#define SERVICE_NAME "Mara X"
#define SERVICE_UUID "46a9920a-b616-44e0-b33e-ac36ab2fe087"          // UART service UUID
#define REALTIME_CHAR_UUID "1b2c8e21-f323-4b9a-82f7-393b63a5917d"    //real time info with notify
#define HXTEMP_HIST_CHAR_UUID "fdaa14a3-5ab5-42b0-96eb-815c2a57b68b" //buffer of hx temp
#define STEMP_HIST_CHAR_UUID "51412868-e2b6-43dc-a10d-cc5a73c0e566"  //buffer of steam temp
#define STTEMP_HIST_CHAR_UUID "0dad858a-033c-46c5-930d-2eac5dd58478" //buffer of steam target temp
#define SETTINGS_UUID "8bab20c8-903e-4423-a186-b68d2411ac09"         //settings

SETTINGS settings = SETTINGS();

BLEServer *pServer = NULL;
BLECharacteristic *pTxCharacteristic;
BLECharacteristic *pHxHistoryCharacteristic;
BLECharacteristic *pSHistoryCharacteristic;
BLECharacteristic *pStHistoryCharacteristic;
BLECharacteristic *pSettingsCharacteristic;

bool bleConnected = false;
uint32_t bleLastNotify = 0;
bool oldBLEDeviceConnected = false;
bool wifiConnected = false; //set to true if wifi connection is established on setup();
String wifiIP = "";

const uint16_t BLE_NOTIFY_PERIOD = 500;

const uint16_t DEMO_DEBUG_LINE_PROCESS_PERIOD = 2000; //we will only process the line every Xms when in demo mode.
const uint16_t DEBUG_LINE_PROCESS_PERIOD = 2000;      //we will only process the line every Xms
const uint16_t DEBUG_LINE_TIMEOUT_WARNING = 5000;     //if there is no line received from the PID for X ms , write a warning on TFT ?
const uint16_t DEBUG_LINE_TIMEOUT_ERROR = 60000;      //if there is no line received from the PID for X ms , black out the tft and pretend to sleep

const uint16_t DISPLAY_REFRESH_PERIOD = 75;       //update the display. 500ms = 2fps, 50ms = 20fps;
                                                  //WARNING... high refresh rates will mess up reed sensor detection logic.
                                                  //100ms seem safe with ESP32 at full speed and 2.4" featherwing 320x240 tft.
                                                  //smaller/faster monitors can go lower if needed
const uint16_t ADAFRUIT_IO_REFRESH_PERIOD = 7000; // 3feeds every 7000ms = ~26upd/min (< free limit) , 3feeds every 6000ms = 30upd/min (free limit), 3 feeds every 3000ms = 60/s (IO+ limit)

const uint8_t GREEN_TEMP_MIN = 89; //determines the green area of the meter anything below MIN will be blue and above MAX red
const uint8_t GREEN_TEMP_MAX = 99; //determines the green area of the meter anything below MIN will be blue and above MAX red

const uint16_t HX_TEMP_BUFFER_LENGTH = 2048;
const uint16_t STEAM_TEMP_BUFFER_LENGTH = 2048;
const uint16_t STEAM_TARGET_TEMP_BUFFER_LENGTH = 2048;

const uint16_t PUMP_DETECTION_DELAY = 500;    //only change to off if we get 0 values throughout the delay
const uint16_t MIN_PUMP_ON_TIME = 500;        //anything below 500ms will not SHOW the timer (internaly the timer will start)
const uint16_t MIN_PUMP_ON_SAVE_TIME = 18000; //anything below 12s will not save the timer

uint32_t serialLastReadMillis = 0;
uint32_t lastDisplayRefreshMillis = 0;
uint32_t lastActionMillis = 0;

bool sleeping = false;

const byte numChars = 32;
char receivedChars[numChars];
char endMarker = '\n';
char rc;

bool pumpEnabled = false;
uint32_t pumpFirstStopMillis = 0;
uint32_t pumpEnabledMillis = 0;
uint32_t lastPumponDuration = 0; //in millis

String modeAndSoftware = "";

uint8_t hxTemp = 0;
uint8_t steamTemp = 0;
uint8_t steamTargetTemp = 0;
bool heatingEnabled = 0;
uint16_t heatingBoostValue = 0;
uint32_t timerMillis = 0;

CircularBuffer<byte, HX_TEMP_BUFFER_LENGTH> hxTempBuff;
CircularBuffer<byte, STEAM_TEMP_BUFFER_LENGTH> steamTempBuff;
CircularBuffer<byte, STEAM_TARGET_TEMP_BUFFER_LENGTH> steamTargetTempBuff;

#if !defined(DEMO)
static byte ndx = 0;
#endif

void saveWifi(std::string bleInput)
{
    String outputString = "ERROR_BLE";
    String bleInputString = bleInput.c_str();
    int INPUT_SIZE = 30;
    char input[INPUT_SIZE + 1];
    bleInputString.toCharArray(input, INPUT_SIZE);
    char *val = strchr(input, ';');
    if (val != 0)
    {
        *val = 0;
        String wifissid = input;
        ++val;
        settings.wifiSSID = wifissid;
        settings.wifiPassword = val;
        settings.autoConnectWiFi = (wifissid.length() > 0) ? 1 : 0;
        settings.saveSettings();
        Serial.print("got ble command");
        ESP.restart();
    }
}

class MyServerCallbacks : public BLEServerCallbacks
{
    void onConnect(BLEServer *pServer)
    {
        bleConnected = true;
    };

    void onDisconnect(BLEServer *pServer)
    {
        bleConnected = false;
    }
};
class bleSettingsCallbacks : public BLECharacteristicCallbacks
{
    void onWrite(BLECharacteristic *pCharacteristic)
    {
        Serial.println("got command from BLE");
        Serial.println(pCharacteristic->getValue().c_str());
        saveWifi(pCharacteristic->getValue());
    }
};

String makeStringGreatAgain(String inputstr, int length)
{
    String output = inputstr;
    for (int i = inputstr.length(); i < length; i++)
    {
        output = " " + output;
    }
    return output;
}

String getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = {0, -1};
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++)
    {
        if (data.charAt(i) == separator || i == maxIndex)
        {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i + 1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

void refreshDisplay()
{
    if (!sleeping || bleConnected)
    {
        if (millis() - lastDisplayRefreshMillis > DISPLAY_REFRESH_PERIOD)
        {
            lastDisplayRefreshMillis += DISPLAY_REFRESH_PERIOD;
            display.drawHxTemp(hxTemp, false, false);
            display.drawSteamTemps(steamTemp, steamTargetTemp);
            display.drawTimers(timerMillis, lastPumponDuration, timerMillis > 0);
            display.drawBleLogo(bleConnected);
            display.drawWiFiLogo(wifiConnected);
            display.drawHeatingIcon(heatingEnabled, heatingBoostValue);
            display.drawSteamBoost(heatingBoostValue);
        }
    }
}

void bleNotify()
{
    if (bleConnected && (millis() - bleLastNotify > BLE_NOTIFY_PERIOD))
    {
        //uint32_t timer, realtime counter in millis of CURRENT shot
        //uint32_t lastPumponDuration, PREVIOUS timer duration
        //String wifiIP , IP of wifi, mainly for debug reasons and for REST API (later). If there is an IP, MCU is connected with WIFI
        //uint16_t heatingBoostValue , a number 0-1500 indicating the boiler "boost" level (not important)
        //byte pumpEnabled, self explanatory
        //byte heatingEnabled, heating element on/off
        //uint8_t hxTemp, temperature of heat exhanger. THE most important temp.
        //uint8_t steamTemp, temp of steam boiler. Not so important
        //uint8_t steamTargetTemp, TARGET temp of steam boiler. The PID will set and try to reach it. It might be <steamTemp though

        bleLastNotify += BLE_NOTIFY_PERIOD;
        String tempjson = "{\"timer\" : " + String(timerMillis) + ", \"lastPumpOnDuration\" : " + String(lastPumponDuration);
        tempjson += ", \"wifiIP\" : \"" + wifiIP + "\"";
        tempjson += ", \"heatingEnabled\" : \"" + String(heatingEnabled) + "\"";
        tempjson += ", \"heatingBoostValue\" : \"" + String(heatingBoostValue) + "\"";
        tempjson += ", \"pumpEnabled\" : \"" + String(pumpEnabled) + "\"";
        tempjson += ", \"hx\" : " + String(hxTemp) + ", \"s\" : " + String(steamTemp) + ", \"st\" : " + String(steamTargetTemp) + "}";

        pTxCharacteristic->setValue(tempjson.c_str());
        pTxCharacteristic->notify();
    }
}

void bleNotifyMin()
{
    if (bleConnected && (millis() - bleLastNotify > BLE_NOTIFY_PERIOD))
    {
        //this is our prefered method...we save some bytes and improve our transmission frequency
        bleLastNotify += BLE_NOTIFY_PERIOD;
        String tempjson = "{\"condensedstr\" : \"" + String(timerMillis) + "," + String(lastPumponDuration);
        tempjson += "," + wifiIP;
        tempjson += "," + String(heatingEnabled);
        tempjson += "," + String(heatingBoostValue);
        tempjson += "," + String(pumpEnabled);
        tempjson += "," + String(hxTemp) + "," + String(steamTemp) + "," + String(steamTargetTemp) + "\"}";

        pTxCharacteristic->setValue(tempjson.c_str());
        pTxCharacteristic->notify();
    }
}

#if defined(USE_ADAFRUIT_IO)
void adafruitIOMQTTRefresh()
{
    if (millis() - lastAdafruitIORefreshMillis > ADAFRUIT_IO_REFRESH_PERIOD)
    {
        lastAdafruitIORefreshMillis += ADAFRUIT_IO_REFRESH_PERIOD;
        Serial.print("will connect and update adafruitio feeds");

        if (!hxtempfeed.publish(hxTemp))
        {
            Serial.println(F("hxTemp failed !"));
        }
        if (!stempfeed.publish(steamTemp))
        {
            Serial.println(F("steamTemp failed !"));
        }
        if (!sttempfeed.publish(steamTargetTemp))
        {
            Serial.println(F("steamTargetTempPublish failed !"));
        }

        if (!mqtt.ping())
        {
            mqtt.disconnect();
        }
    }
}

void MQTT_connect()
{
    int8_t ret;

    // Stop if already connected.
    if (mqtt.connected())
    {
        return;
    }

    Serial.print("Connecting to MQTT... ");

    uint8_t retries = 3;
    while ((ret = mqtt.connect()) != 0)
    { // connect will return 0 for connected
        Serial.println(mqtt.connectErrorString(ret));
        Serial.println("Retrying MQTT connection in 5 seconds...");
        mqtt.disconnect();
        delay(5000); // wait 5 seconds
        retries--;
        if (retries == 0)
        {
            // basically die and wait for WDT to reset me
            while (1)
                ;
        }
    }
    Serial.println("MQTT Connected!");
}
#endif

#if !defined(DEMO)
void processSerialDebugLine(char *input)
{
    // https://www.home-barista.com/espresso-machines/lelit-marax-data-visualisation-mod-t66187.html
    // C1.19,116,124,095,0560,0
    // C1.19 - C for Coffee, 1.19 is the software version. This can also be V for Vapour (Steam) if the machine is in Steam priority mode.
    // 116 - This is the real/actual steam temperature in Celsius.
    // 124 - This is the target steam temperature in Celsius.
    // 096 - This is the real/actual heat exchanger temperature in Celsius.
    // 0560 - This is a countdown used to track if the machine is in "fast heating" mode, it seems to go anywhere from 1500-0. 0 means it's no longer boosting.
    // 0 - This represents if the heating element is on, 0 is Off, 1is On.

    if (millis() - serialLastReadMillis > DEBUG_LINE_PROCESS_PERIOD)
    {
        Serial.println("tick");
        serialLastReadMillis += DEBUG_LINE_PROCESS_PERIOD;

        modeAndSoftware = getValue(input, ',', 0);
        steamTemp = getValue(input, ',', 1).toInt();
        steamTargetTemp = getValue(input, ',', 2).toInt();
        hxTemp = getValue(input, ',', 3).toInt();
        heatingBoostValue = getValue(input, ',', 4).toInt();
        heatingEnabled = getValue(input, ',', 5).toInt();

        hxTempBuff.unshift(hxTemp);
        steamTempBuff.unshift(steamTemp);
        steamTargetTempBuff.unshift(steamTargetTemp);

        if (bleConnected)
        {
            int blebuffsize = 120;
            if (hxTempBuff.size() < blebuffsize)
            {
                blebuffsize = hxTempBuff.size();
            }
            byte hxbuff[blebuffsize];
            for (byte i = 0; i < blebuffsize; i++)
            {
                hxbuff[i] = hxTempBuff[i];
            }
            pHxHistoryCharacteristic->setValue(hxbuff, blebuffsize);
        }

        // String tempjson = "{\"timer\" : " + String(timerMillis) + ", \"lastPumpOnDuration\" : " + String(lastPumponDuration);
        // tempjson += ", \"wifiIP\" : \"" + wifiIP + "\"";
        // tempjson += ", \"hx\" : " + String(hxTemp) + ", \"s\" : " + String(steamTemp) + ", \"st\" : " + String(steamTargetTemp) + "}";
    }
}
void getMachineInput()
{
    while (Serial1.available())
    {
        rc = Serial1.read();

        if (rc != endMarker)
        {
            receivedChars[ndx] = rc;
            ndx++;
            if (ndx >= numChars)
            {
                ndx = numChars - 1;
            }
        }
        else
        {
            lastActionMillis = millis();
            receivedChars[ndx] = '\0';
            Serial.print("full buffer = ");
            Serial.println(receivedChars);
            processSerialDebugLine(receivedChars);
            ndx = 0;
        }
    }
    if (!sleeping && !bleConnected)
    {
        const uint32_t debuglinetimeout = millis() - lastActionMillis;
        if (!pumpEnabled && (debuglinetimeout > DEBUG_LINE_TIMEOUT_ERROR))
        {
            Serial.println("bye bye");
            sleeping = true;
            display.sleep();
        }
        else if (debuglinetimeout > DEBUG_LINE_TIMEOUT_WARNING)
        {
            hxTemp = 0;
            steamTemp = 0;
            steamTargetTemp = 0;
        }
    }
}
#endif
#if defined(DEMO)
void demoDisplay()
{
    static int temp = 85;
    static bool inc = true;
    if (millis() - serialLastReadMillis > DEMO_DEBUG_LINE_PROCESS_PERIOD)
    {
        serialLastReadMillis += DEMO_DEBUG_LINE_PROCESS_PERIOD;
        if (inc)
        {
            if (temp < 110)
            {
                temp++;
            }
            else
            {
                inc = false;
                temp--;
            }
        }
        else
        {
            if (temp < 90)
            {
                inc = true;
                temp++;
            }
            else
            {
                temp--;
            }
        }
        hxTemp = temp;
        steamTemp = hxTemp + 20;
        steamTargetTemp = hxTemp + 25;
        hxTempBuff.unshift(hxTemp);
        steamTempBuff.unshift(steamTemp);
        steamTargetTempBuff.unshift(steamTargetTemp);
        heatingEnabled = 1;
        if (hxTemp * 5 > 510)
        {
            heatingBoostValue = 1002;
        }
        else
        {
            heatingBoostValue = hxTemp * 5;
        }

        if (bleConnected)
        {
            int blebuffsize = 120;
            if (hxTempBuff.size() < blebuffsize)
            {
                blebuffsize = hxTempBuff.size();
            }
            byte hxbuff[blebuffsize];
            for (byte i = 0; i < blebuffsize; i++)
            {
                hxbuff[i] = hxTempBuff[i];
            }
            pHxHistoryCharacteristic->setValue(hxbuff, blebuffsize);
        }
        // bleNotify();
    }
}

#endif

void bleHandleDisconnect()
{
    // disconnecting
    if (!bleConnected && oldBLEDeviceConnected)
    {
        delay(500);                  // give the bluetooth stack the chance to get things ready
        pServer->startAdvertising(); // restart advertising
        Serial.println("start advertising");
        oldBLEDeviceConnected = bleConnected;
    }
    // connecting
    if (bleConnected && !oldBLEDeviceConnected)
    {
        // do stuff here on connecting
        oldBLEDeviceConnected = bleConnected;
    }
}

void setup()
{
    Serial1.begin(9600);
    Serial.begin(115200);

    pinMode(PUMP_PIN, RESISTOR_TYPE);
    pumpEnabled = false;

    Serial.println("Mara X");

    display.initialize();
    display.printInfo();
    if (!settings.loadSettings())
    {
        Serial.println("resetting all settings");
        settings.resetSettings();
        settings.loadSettings();
    }

    BLEDevice::init(SERVICE_NAME);
    bleLastNotify = 0;
    // Create the BLE Server
    pServer = BLEDevice::createServer();
    pServer->setCallbacks(new MyServerCallbacks());
    // Create the BLE Service
    BLEService *pService = pServer->createService(SERVICE_UUID);

    // Create BLE Characteristics
    pTxCharacteristic = pService->createCharacteristic(
        REALTIME_CHAR_UUID,
        BLECharacteristic::PROPERTY_NOTIFY);
    pHxHistoryCharacteristic = pService->createCharacteristic(
        HXTEMP_HIST_CHAR_UUID,
        BLECharacteristic::PROPERTY_READ);
    pSHistoryCharacteristic = pService->createCharacteristic(
        STEMP_HIST_CHAR_UUID,
        BLECharacteristic::PROPERTY_READ);
    pStHistoryCharacteristic = pService->createCharacteristic(
        STTEMP_HIST_CHAR_UUID,
        BLECharacteristic::PROPERTY_READ);
    pSettingsCharacteristic = pService->createCharacteristic(
        SETTINGS_UUID,
        BLECharacteristic::PROPERTY_WRITE);

    pSettingsCharacteristic->setCallbacks(new bleSettingsCallbacks());

    pTxCharacteristic->addDescriptor(new BLE2902());
    pHxHistoryCharacteristic->addDescriptor(new BLE2902());
    pSHistoryCharacteristic->addDescriptor(new BLE2902());
    pStHistoryCharacteristic->addDescriptor(new BLE2902());
    pSettingsCharacteristic->addDescriptor(new BLE2902());
    // Start the service
    pService->start();

    // Start advertising
    pServer->getAdvertising()->start();
    Serial.println("Waiting a client connection to notify...");

#if defined(USE_WIFI)
    wifiIP = "";
    wifiActivated = false;

    if (settings.wifiSSID.length() > 0 && settings.wifiPassword.length() > 0)
    {
        Serial.print("connecting to wifi network...");
        Serial.print(settings.wifiSSID.c_str());
        Serial.println("...");
        WiFi.begin(settings.wifiSSID.c_str(), settings.wifiPassword.c_str());
        uint32_t wifiConnectionStart = millis();
        bool abortConnection = false;
        // Wait for connection
        while (!abortConnection && WiFi.status() != WL_CONNECTED)
        {
            delay(500);
            yield();
            Serial.print(".");
            if (millis() - wifiConnectionStart > WIFI_CONNECTION_TIMEOUT * 1000)
            {
                Serial.println("wifi connection timed out...");
                abortConnection = true;
            }
        }
        if (!abortConnection)
        {
            Serial.println("");
            Serial.print("Connected to ");
            Serial.print(settings.wifiSSID);
            Serial.print("IP address: ");
            Serial.println(WiFi.localIP().toString());
            wifiIP = WiFi.localIP().toString();
            wifiActivated = true;
        }
    }

#endif

    display.blank();
    display.wakeup();
}

void calculateTimer(int inputValue)
{
    uint32_t timepumpEnabled = millis() - pumpEnabledMillis;
    if (!inputValue)
    {
        //pump is on !!!
        if (sleeping)
        {
            sleeping = false;
            display.wakeup();
        }
        lastActionMillis = millis();
        pumpFirstStopMillis = 0;
        if (!pumpEnabled)
        {
            display.drawPumpIcon(true);
            pumpEnabled = true;
            pumpEnabledMillis = millis();
        }
    }
    else
    {

        if (pumpEnabled)
        {
            bool stopTimer = false;
            if (IS_REED)
            {
                //we cannot disable immediately since reed pulses between on/off...
                //let's wait PUMP_DETECTION_DELAY (see: https://github.com/alexrus/marax_timer)
                if (pumpFirstStopMillis == 0)
                {
                    //our 1st detection of off...
                    pumpFirstStopMillis = millis();
                }
                else
                {
                    if (millis() - pumpFirstStopMillis > PUMP_DETECTION_DELAY)
                    {
                        //we passed our threshold...pump is definitely off...
                        stopTimer = true;
                        //but...since we we waited 1 period, we need to remove this amount of time from our timer
                        timepumpEnabled -= PUMP_DETECTION_DELAY;
                    }
                }
            }
            if (stopTimer)
            {
                display.drawPumpIcon(false);
                lastActionMillis = millis();
                pumpEnabled = false;
                pumpEnabledMillis = 0;
                pumpFirstStopMillis = 0;
                timerMillis = 0;
                if (timepumpEnabled > MIN_PUMP_ON_SAVE_TIME)
                {
                    lastPumponDuration = timepumpEnabled;
                }
            }
        }
    }

    if (pumpEnabledMillis > 0 && timepumpEnabled > MIN_PUMP_ON_TIME)
    {
        timerMillis = millis() - pumpEnabledMillis;
    }
}

void loop()
{
    calculateTimer(digitalRead(PUMP_PIN));
#if defined(USE_WIFI)
    if (WiFi.isConnected())
    {
        wifiConnected = true;
    }
    else
    {
        wifiConnected = false;
    }
#endif

#if defined(USE_ADAFRUIT_IO)
    if (wifiConnected)
    {
        MQTT_connect();
    }
#endif

#if defined(DEMO)
    demoDisplay();
#else
    getMachineInput();
#endif

    // bleNotify();
    bleNotifyMin();
    bleHandleDisconnect();
    refreshDisplay();
#if defined(USE_ADAFRUIT_IO)
    if (mqtt.connected())
    {
        adafruitIOMQTTRefresh();
    }
#endif
}