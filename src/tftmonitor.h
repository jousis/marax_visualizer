#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>

#define STMPE_CS 32 //touch
#define SD_CS 14    //sd
#define TFT_CS 15
#define TFT_DC 33

#define DISPLAY_WHITE 0xFFFF
#define DISPLAY_BLACK 0x0000
#define DISPLAY_YELLOW 0xFFE0

class TFT_MONITOR
{
public:
    TFT_MONITOR();

    void initialize();
    void printInfo();
    void blank();
    void sleep();
    void wakeup();
    void drawScaleRing();
    void drawSolidRingMeter();
    void drawHxTemp(int value, bool skipText, bool forceRedraw);

    // void drawSteamBoilerTemps(int realTemp, int targetTemp); //DEPR
    void drawSteamTemps(int realTemp, int targetTemp);
    void drawSteamTempsRect();

    void drawAngle(int angle, int v, int r, int w, int x, int y);
    void drawTimers(uint32_t realtime, uint32_t last, bool priorityOnRealtime);
    void drawBleLogo(bool show);
    void drawWiFiLogo(bool show);
    void drawHeatingIcon(bool heatingEnabled, uint16_t heatingBoostValue);
    void drawSteamBoost(uint16_t heatingBoostValue);
    void drawText(String inputString, uint8_t textSize, uint16_t color, uint16_t bg_color, int16_t x, int16_t y);
    void drawPumpIcon(bool status);

protected:
    const uint16_t BREW_TEMP_COLOR = ILI9341_GREEN;
    const uint8_t GREEN_TEMP_MIN = 88; //determines the green area of the meter anything below MIN will be blue and above MAX red
    const uint8_t GREEN_TEMP_MAX = 98; //determines the green area of the meter anything below MIN will be blue and above MAX red

    const int hxTempScaleX = 205;
    const int hxTempScaleY = 200;
    const int hxTempScaleRadius = 105;
    const int hxTempScaleWidth = 10;

    const int hxTempDialMinAngle = -110; //ring starts at -110 degrees
    const int hxTempDialMaxAngle = 110;  //ring ends at 110 degrees

    const int hxTempDialMinValue = 23;  //23C is the minimum of the ring
    const int hxTempDialMaxValue = 130; //120C is the maximum of the ring

    int lastV = hxTempDialMaxAngle; //lastV so we can track which region we need to redraw
    int vEndOfGreen = 0;

    const int hxTempDialX = 105;
    const int hxTempDialY = 100;
    const int hxTempDialRadius = 100;
    const int hxTempDialWidth = 40;

    const int SteamTempRectMin = 23;
    const int SteamTempRectMax = 200;
    const int SteamTempRectX = 5;
    const int SteamTempRectY = 35;
    const int SteamTempRectWidth = 38;
    const int SteamTempRectHeight = 205;

    const int bleLogoX = 290;
    const int bleLogoY = 2;
    const int wifiLogoX = 255;
    const int wifiLogoY = 2;
    const int heatingIconX = 9;
    const int heatingIconY = 2;

    String makeStringGreatAgain(String inputstr, int length);
    Adafruit_ILI9341 display = Adafruit_ILI9341(TFT_CS, TFT_DC);
};