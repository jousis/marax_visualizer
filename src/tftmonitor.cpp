#include <tftmonitor.h>
#include <images.h>

TFT_MONITOR::TFT_MONITOR()
{
}

void TFT_MONITOR::initialize()
{
    display.begin();
    display.setRotation(1);
    display.fillScreen(ILI9341_BLACK);
    vEndOfGreen = map(GREEN_TEMP_MAX, hxTempDialMinValue, hxTempDialMaxValue, hxTempDialMinAngle, hxTempDialMaxAngle); // Map the max green to an angle v
};

void TFT_MONITOR::printInfo()
{
    drawText("Lelit Mara X", 4, DISPLAY_WHITE, DISPLAY_BLACK, 10, 60);
    drawText("#OpenSource", 2, DISPLAY_WHITE, DISPLAY_BLACK, 5, 140);
    drawText("#IoT", 2, DISPLAY_WHITE, DISPLAY_BLACK, 5, 160);
    drawText("#ESP32", 2, DISPLAY_WHITE, DISPLAY_BLACK, 5, 180);
    drawText("https://gitlab.com/jousis/marax_visualizer", 1, DISPLAY_WHITE, DISPLAY_BLACK, 5, 200);
}

void TFT_MONITOR::blank()
{
    display.fillScreen(ILI9341_BLACK);
};

void TFT_MONITOR::sleep()
{
    display.fillScreen(ILI9341_BLACK);
};

void TFT_MONITOR::wakeup()
{
    lastV = hxTempDialMaxAngle;
    drawScaleRing();
    drawSolidRingMeter();
    lastV = hxTempDialMinAngle;
    drawSteamTempsRect();
};

void TFT_MONITOR::drawText(String inputString, uint8_t textSize, uint16_t color, uint16_t bg_color, int16_t x, int16_t y)
{
    display.setCursor(x, y);
    display.setTextSize(textSize);
    display.setTextColor(color, bg_color);
    display.println(inputString);
}

String TFT_MONITOR::makeStringGreatAgain(String inputstr, int length)
{
    String output = inputstr;
    for (int i = inputstr.length(); i < length; i++)
    {
        output = " " + output;
    }
    return output;
}

void TFT_MONITOR::drawScaleRing()
{
    int minArc = -110;
    int maxArc = 105;
    int greenStart = map(GREEN_TEMP_MIN, hxTempDialMinValue, hxTempDialMaxValue, minArc, maxArc);
    int greenEnd = map(GREEN_TEMP_MAX, hxTempDialMinValue, hxTempDialMaxValue, minArc, maxArc);
    int x = hxTempScaleX;
    int y = hxTempScaleY;
    int r = hxTempScaleRadius;
    int width = hxTempScaleWidth;

    for (int i = minArc; i <= maxArc; i += 5)
    {

        float sx = cos((i - 90) * 0.0174532925);
        float sy = sin((i - 90) * 0.0174532925);
        uint16_t x0 = sx * (r + width) + x;
        uint16_t y0 = sy * (r + width) + y;
        uint16_t x1 = sx * r + x;
        uint16_t y1 = sy * r + y;
        float sx2 = cos((i + 5 - 90) * 0.0174532925);
        float sy2 = sin((i + 5 - 90) * 0.0174532925);
        int x2 = sx2 * (r + width) + x;
        int y2 = sy2 * (r + width) + y;
        int x3 = sx2 * r + x;
        int y3 = sy2 * r + y;

        if (i >= minArc && i < greenStart)
        {
            display.fillTriangle(x0, y0, x1, y1, x2, y2, ILI9341_BLUE);
            display.fillTriangle(x1, y1, x2, y2, x3, y3, ILI9341_BLUE);
        }

        if (i >= greenStart && i <= greenEnd)
        {
            display.fillTriangle(x0, y0, x1, y1, x2, y2, ILI9341_GREEN);
            display.fillTriangle(x1, y1, x2, y2, x3, y3, ILI9341_GREEN);
        }

        // Orange zone limits
        if (i > greenEnd && i <= maxArc)
        {
            display.fillTriangle(x0, y0, x1, y1, x2, y2, ILI9341_RED);
            display.fillTriangle(x1, y1, x2, y2, x3, y3, ILI9341_RED);
        }
    }
}

void TFT_MONITOR::drawAngle(int angle, int v, int r, int w, int x, int y)
{
    uint16_t color = ILI9341_GREEN;
    const byte seg = 1;

    // Calculate pair of coordinates for segment start
    float sx = cos((angle - 90) * 0.0174532925);
    float sy = sin((angle - 90) * 0.0174532925);
    uint16_t x0 = sx * (r - w) + x;
    uint16_t y0 = sy * (r - w) + y;
    uint16_t x1 = sx * r + x;
    uint16_t y1 = sy * r + y;

    // Calculate pair of coordinates for segment end
    float sx2 = cos((angle + seg - 90) * 0.0174532925);
    float sy2 = sin((angle + seg - 90) * 0.0174532925);
    int x2 = sx2 * (r - w) + x;
    int y2 = sy2 * (r - w) + y;
    int x3 = sx2 * r + x;
    int y3 = sy2 * r + y;

    if (angle < v)
    {
        if (angle > vEndOfGreen)
        {
            color = ILI9341_RED;
        }
        display.fillTriangle(x0, y0, x1, y1, x2, y2, color);
        display.fillTriangle(x1, y1, x2, y2, x3, y3, color);
    }
    else
    {
        display.fillTriangle(x0, y0, x1, y1, x2, y2, ILI9341_DARKGREY);
        display.fillTriangle(x1, y1, x2, y2, x3, y3, ILI9341_DARKGREY);
    }
}

void TFT_MONITOR::drawSolidRingMeter()
{
    const int x = hxTempDialX + hxTempDialRadius;
    const int y = hxTempDialY + hxTempDialRadius;

    for (int i = hxTempDialMinAngle; i < hxTempDialMaxAngle; i++)
    {
        drawAngle(i, hxTempDialMinAngle, hxTempDialRadius, hxTempDialWidth, x, y);
    }
}

void TFT_MONITOR::drawHxTemp(int value, bool skipText, bool forceRedraw)
// https: //www.instructables.com/Arduino-analogue-ring-meter-on-colour-TFT-display/
{
    const int x = hxTempDialX + hxTempDialRadius;
    const int y = hxTempDialY + hxTempDialRadius;
    String valueText = makeStringGreatAgain(String(value), 3) + "c";

    if (value < hxTempDialMinValue)
    {
        valueText = makeStringGreatAgain(" ", 4);
        value = hxTempDialMinValue;
    }
    else if (value > hxTempDialMaxValue)
    {
        valueText = makeStringGreatAgain(String(hxTempDialMaxValue), 3) + "c";
        value = hxTempDialMaxValue;
    }

    int v = map(value, hxTempDialMinValue, hxTempDialMaxValue, hxTempDialMinAngle, hxTempDialMaxAngle); // Map the value to an angle v

    if (lastV == v && !forceRedraw)
    {
        return;
    }

    int min = lastV;
    int max = v;

    //we must redraw from lastV to V each time
    //a full redraw costs a lot of time (>120ms)
    if (v < lastV)
    {
        min = v;
        max = lastV;
    }

    for (int i = min; i < max; i++)
    {
        drawAngle(i, v, hxTempDialRadius, hxTempDialWidth, x, y);
    }

    lastV = v;
    int offset = 45;
    if (!skipText)
    {
        // Set the text colour to default
        display.setTextColor(ILI9341_WHITE, ILI9341_BLACK);
        // Print value
        display.setTextSize(4);
        display.setCursor(x - offset, y - 20);
        display.print(valueText);
    }
}

void TFT_MONITOR::drawSteamTempsRect()
{
    drawSteamTemps(0, 0);
    drawSteamBoost(0);
}

void TFT_MONITOR::drawSteamTemps(int realTemp, int targetTemp)
{
    //in general... from SteamTempRectY to Y = ILI9341_DARKGREY , from Y to SteamTempRectHeight ILI9341_ORANGE
    //for efficiency, we will track the oldY (previous value)
    //if oldY = newY => do nothing
    //if newY > oldY  , SteamTempRectY -> newY and oldY->SteamTempRectHeight = unchanged, oldY->newY = ILI9341_DARKGREY
    //if oldY > newY , SteamTempRectY -> oldY and newY->SteamTempRectHeight = unchanged, newY->oldY = ILI9341_ORANGE
    //so, in reality, we only care about the rect with Y min(oldY,newY) to max(oldY,minY)

    static int oldY = SteamTempRectY;
    static int oldTargetY = SteamTempRectY;

    int newY = map(realTemp, SteamTempRectMin, SteamTempRectMax, SteamTempRectY + SteamTempRectHeight, SteamTempRectY);
    int newTargetY = map(targetTemp, SteamTempRectMin, SteamTempRectMax, SteamTempRectY + SteamTempRectHeight, SteamTempRectY);

    if (newY == oldY && newTargetY == oldTargetY)
    {
        return;
    }
    if (newY > oldY)
    {
        display.fillRect(SteamTempRectX, oldY, SteamTempRectWidth, newY - oldY, ILI9341_DARKGREY);
    }
    else
    {
        display.fillRect(SteamTempRectX, newY, SteamTempRectWidth, oldY - newY, ILI9341_ORANGE);
    }
    if (newTargetY != oldTargetY)
    {
        display.fillRect(SteamTempRectX + SteamTempRectWidth, oldTargetY - 8, 38, 16, ILI9341_BLACK);
        // display.fillRect(SteamTempRectX + SteamTempRectWidth, newTargetY - 2, 4, 4, ILI9341_RED); //a small rectangle where the target temp is
        display.setCursor(SteamTempRectX + SteamTempRectWidth + 4, newTargetY - 8);
        display.setTextSize(2);
        display.setTextColor(ILI9341_RED, ILI9341_BLACK);
        display.print(makeStringGreatAgain(String(targetTemp), 3));
    }
    oldY = newY;
    oldTargetY = newTargetY;
    display.setCursor(SteamTempRectX + 2, SteamTempRectY + SteamTempRectHeight - 15);
    display.setTextSize(2);
    display.setTextColor(ILI9341_BLACK, ILI9341_ORANGE);
    display.print(makeStringGreatAgain(String(realTemp), 3));
}

void TFT_MONITOR::drawSteamBoost(uint16_t heatingBoostValue)
{
    static int oldY = SteamTempRectY + SteamTempRectHeight;

    int newY = map(heatingBoostValue, 0, 1500, SteamTempRectY + SteamTempRectHeight, SteamTempRectY);

    if (newY == oldY)
    {
        return;
    }
    if (newY > oldY)
    {
        display.fillRect(0, oldY, 3, newY - oldY, ILI9341_BLACK);
    }
    else
    {
        display.fillRect(0, newY, 3, oldY - newY, ILI9341_RED);
    }
    oldY = newY;
}

void TFT_MONITOR::drawTimers(uint32_t realtime, uint32_t lastTimer, bool priorityOnRealtime)
{
    static uint32_t oldRT = 0;
    static uint32_t oldLT = 0;
    static bool oldPR = true;

    if (realtime == oldRT && lastTimer == oldLT && priorityOnRealtime == oldPR)
    {
        return;
    }

    float bigTimerFloat;
    float smallTimerFloat;

    if (priorityOnRealtime)
    {
        bigTimerFloat = (float)realtime / 1000;
        smallTimerFloat = (float)lastTimer / 1000;
    }
    else
    {
        bigTimerFloat = (float)lastTimer / 1000;
        smallTimerFloat = (float)realtime / 1000;
    }

    int xPrimaryText = SteamTempRectX + SteamTempRectWidth;
    int xSecondaryText = SteamTempRectX + SteamTempRectWidth + 150;
    int yPrimaryText = 35;
    int ySecondaryText = 45;

    display.setTextColor(ILI9341_WHITE, ILI9341_BLACK);
    display.setTextSize(4);
    display.setCursor(xPrimaryText, yPrimaryText);
    display.print(makeStringGreatAgain(String(bigTimerFloat, 1), 5));

    display.setTextColor(ILI9341_WHITE, ILI9341_BLACK);
    display.setTextSize(2);
    display.setCursor(xSecondaryText, ySecondaryText);
    display.print(makeStringGreatAgain(String(smallTimerFloat, 1), 5));

    if (!priorityOnRealtime)
    {
        display.fillRect(xSecondaryText + 8, ySecondaryText - 15, 60, 10, ILI9341_BLACK);
        display.setTextColor(ILI9341_GREEN, ILI9341_BLACK);
        display.setTextSize(2);
        display.setCursor(xPrimaryText + 25, yPrimaryText - 20);
        display.print("LAST SHOT");
    }
    else
    {
        display.fillRect(xPrimaryText + 25, yPrimaryText - 20, 120, 18, ILI9341_BLACK);
        display.setTextColor(ILI9341_GREEN, ILI9341_BLACK);
        display.setTextSize(1);
        display.setCursor(xSecondaryText + 8, ySecondaryText - 15);
        display.print("LAST SHOT");
    }

    oldRT = realtime;
    oldLT = lastTimer;
    oldPR = priorityOnRealtime;
}
void TFT_MONITOR::drawPumpIcon(bool status)
{
    if (status)
    {
        display.fillCircle(wifiLogoX - 16, wifiLogoY + 16, 8, ILI9341_GREENYELLOW);
        // display.fillcircle(wifiLogoX-32, wifiLogoY-32, wifiLogo, 32, 32, ILI9341_WHITE);
    }
    else
    {
        display.fillCircle(wifiLogoX - 16, wifiLogoY + 16, 8, ILI9341_BLACK);
    }
}

void TFT_MONITOR::drawWiFiLogo(bool show)
{
    static bool wifiLogoVisible = false;
    if (show && !wifiLogoVisible)
    {
        display.drawBitmap(wifiLogoX, wifiLogoY, wifiLogo, 32, 32, ILI9341_WHITE);
        wifiLogoVisible = true;
        Serial.println("showing wifi logo");
    }
    if (!show && wifiLogoVisible)
    {
        display.fillRect(wifiLogoX, wifiLogoY, 32, 32, ILI9341_BLACK);
        wifiLogoVisible = false;
        Serial.println("hiding wifi logo");
    }
}

void TFT_MONITOR::drawBleLogo(bool show)
{
    static bool bleLogoVisible = false;
    if (show && !bleLogoVisible)
    {
        display.drawBitmap(bleLogoX, bleLogoY, bleLogo, 32, 32, ILI9341_BLUE);
        bleLogoVisible = true;
    }
    if (!show && bleLogoVisible)
    {
        display.fillRect(bleLogoX, bleLogoY, 32, 32, ILI9341_BLACK);
        bleLogoVisible = false;
    }
}

void TFT_MONITOR::drawHeatingIcon(bool heatingEnabled, uint16_t heatingBoostValue)
{
    static int heatingLogoStyle = 0; //0 = disabled, 1 = orange single , 2 = red single, 3 = red+orange stacked
    if (heatingEnabled)
    {
        if (heatingBoostValue > 1000 && heatingLogoStyle != 3)
        {
            heatingLogoStyle = 3;
            display.fillRect(heatingIconX, heatingIconY, 32, 32, ILI9341_BLACK);
            display.drawBitmap(heatingIconX, heatingIconY, heatingLogo, 32, 32, ILI9341_RED);
            display.drawBitmap(heatingIconX + 10, heatingIconY + 16, heatingLogoSmall, 12, 12, ILI9341_YELLOW);
        }
        else if (heatingBoostValue < 500 && heatingLogoStyle != 1)
        {
            heatingLogoStyle = 1;
            display.fillRect(heatingIconX, heatingIconY, 32, 32, ILI9341_BLACK);
            display.drawBitmap(heatingIconX, heatingIconY, heatingLogo, 32, 32, ILI9341_ORANGE);
        }
        else if ((heatingBoostValue >= 500 && heatingBoostValue <= 1000) && heatingLogoStyle != 2)
        {
            heatingLogoStyle = 2;
            display.fillRect(heatingIconX, heatingIconY, 32, 32, ILI9341_BLACK);
            display.drawBitmap(heatingIconX, heatingIconY, heatingLogo, 32, 32, ILI9341_RED);
        }
    }
    if (!heatingEnabled && heatingLogoStyle > 0)
    {
        heatingLogoStyle = 0;
        display.fillRect(heatingIconX, heatingIconY, 32, 32, ILI9341_BLACK);
    }
}
