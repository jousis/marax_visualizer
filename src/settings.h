#include <Preferences.h>

class SETTINGS
{
public:
    SETTINGS();
    byte autoConnectWiFi = 0;
    String wifiHostname = DEFAULT_WIFI_HOSTNAME;
    String wifiSSID = DEFAULT_WIFI_SSID;
    String wifiPassword = DEFAULT_WIFI_PASSWORD;

    void clearEEPROM();
    void resetSettings();
    bool loadSettings();
    void saveSettingByte(const char *key, uint8_t value);
    void saveSettingString(const char *key, String value);
    void saveSettings();

protected:
    Preferences preferences;
    const String DEFAULT_WIFI_HOSTNAME = "MaraX";
    const String DEFAULT_WIFI_SSID = "";
    const String DEFAULT_WIFI_PASSWORD = "";
};